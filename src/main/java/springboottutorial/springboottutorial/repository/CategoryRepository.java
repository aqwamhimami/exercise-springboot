package springboottutorial.springboottutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import springboottutorial.springboottutorial.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    
}
