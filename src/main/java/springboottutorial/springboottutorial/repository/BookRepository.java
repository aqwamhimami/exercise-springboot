package springboottutorial.springboottutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import springboottutorial.springboottutorial.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    
}
