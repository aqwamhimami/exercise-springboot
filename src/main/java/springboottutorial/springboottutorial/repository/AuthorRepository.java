package springboottutorial.springboottutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import springboottutorial.springboottutorial.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    
}
