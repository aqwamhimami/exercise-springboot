package springboottutorial.springboottutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import springboottutorial.springboottutorial.model.Grade;

@Repository
public interface GradeRepository extends JpaRepository<Grade, Long> {
    
}
