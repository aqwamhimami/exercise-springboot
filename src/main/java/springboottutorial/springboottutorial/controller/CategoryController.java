package springboottutorial.springboottutorial.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboottutorial.springboottutorial.exception.ResourceNotFoundException;
import springboottutorial.springboottutorial.model.Category;
import springboottutorial.springboottutorial.repository.CategoryRepository;


@RestController
@RequestMapping("/api/categories")
public class CategoryController{
    
    @Autowired
    CategoryRepository categoryRepository;

    // GET ALL
    @GetMapping("/getAll")
    public List<Category> getAllCategory() {
        return categoryRepository.findAll();
    } 

    // ADD NEW
    @PostMapping("/create")
    public Category createCategory(@Valid @RequestBody Category category) {
        return categoryRepository.save(category);
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Category getOneCategoryById(@PathVariable(value = "id") Long categoryId){
        return categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", categoryId));
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Category updateCategory(@PathVariable(value = "id") Long categoryId,
                                            @Valid @RequestBody Category categoryDetails) {

        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", categoryId));

        category.setCategoryName(categoryDetails.getCategoryName());
        category.setCategoryDescription(categoryDetails.getCategoryDescription());

        Category updateCategory = categoryRepository.save(category);
        return updateCategory;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable(value = "id") Long categoryId) {
        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", categoryId));

        categoryRepository.delete(category);

        return ResponseEntity.ok().build();
    }
}