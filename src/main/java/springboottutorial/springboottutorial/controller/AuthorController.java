package springboottutorial.springboottutorial.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboottutorial.springboottutorial.exception.ResourceNotFoundException;
import springboottutorial.springboottutorial.model.Author;
import springboottutorial.springboottutorial.repository.AuthorRepository;

@RestController
@RequestMapping("/api/authors")
public class AuthorController{
    
    @Autowired
    AuthorRepository authorRepository;

    // GET ALL
    @GetMapping("/getAll")
    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    } 

    // ADD NEW
    @PostMapping("/create")
    public Author createBook(@Valid @RequestBody Author author) {
        return authorRepository.save(author);
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Author getOneAuthorById(@PathVariable(value = "id") Long authorId){
        return authorRepository.findById(authorId)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Author updateNote(@PathVariable(value = "id") Long authorId,
                                            @Valid @RequestBody Author authorDetails) {

        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", authorId));

        author.setFirstName(authorDetails.getFirstName());
        author.setLastName(authorDetails.getLastName());

        Author updateAuthor = authorRepository.save(author);
        return updateAuthor;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable(value = "id") Long authorId) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));

        authorRepository.delete(author);

        return ResponseEntity.ok().build();
    }
}