package springboottutorial.springboottutorial.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboottutorial.springboottutorial.exception.ResourceNotFoundException;
import springboottutorial.springboottutorial.model.Publisher;
import springboottutorial.springboottutorial.repository.PublisherRepository;

@RestController
@RequestMapping("/api/publishers")
public class PublisherController{
    
    @Autowired
    PublisherRepository publisherRepository ;

    // GET ALL
    @GetMapping("/getAll")
    public List<Publisher> getAllPublisher() {
        return publisherRepository.findAll();
    } 

    // ADD NEW
    @PostMapping("/create")
    public Publisher createPublisher(@Valid @RequestBody Publisher publisher) {
        return publisherRepository.save(publisher);
    }
    
    // GET ONE
    @GetMapping("/getOne/{id}")
    public Publisher getOnePublisherById(@PathVariable(value = "id") Long publisherId){
        return publisherRepository.findById(publisherId)
                .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));
    }

    // UPDATE
    @PutMapping("/update/{id}")
    public Publisher updatePublisher(@PathVariable(value = "id") Long publihserId,
                                            @Valid @RequestBody Publisher publisherDetail) {

        Publisher publisher = publisherRepository.findById(publihserId)
                .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publihserId));
        publisher.setCountry(publisherDetail.getCountry());
        publisher.setPublisherName(publisherDetail.getPublisherName());
        publisher.setGrade(publisherDetail.getGrade());

        Publisher updatePublisher = publisherRepository.save(publisher);
        return updatePublisher;
    }

    // DELETE
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deletePublisher(@PathVariable(value = "id") Long publisherId) {
        Publisher publisher = publisherRepository.findById(publisherId)
                .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));

        publisherRepository.delete(publisher);

        return ResponseEntity.ok().build();
    }
}