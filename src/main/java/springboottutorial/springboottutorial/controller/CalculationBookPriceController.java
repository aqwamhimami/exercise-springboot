package springboottutorial.springboottutorial.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboottutorial.springboottutorial.model.Book;
import springboottutorial.springboottutorial.repository.BookRepository;

@RestController
@RequestMapping("/api/books/price")
public class CalculationBookPriceController{
    
    @Autowired
    BookRepository bookRepository;

    // UPDATE
    @PutMapping("/update")
    public HashMap<String, Object> updateBookPrice() {
        List<Book> book = bookRepository.findAll();
        
        for (Book bookDetail : book) {
            double price, basePrice;
            basePrice = (double)bookDetail.getPublisher().getGrade().getBaseProduction();
            price = 1.5 *basePrice;
            bookDetail.setPrice(price);
            bookRepository.save(bookDetail);
        }

        HashMap<String, Object> messageSuccess = new HashMap<String, Object>();
        messageSuccess.put(".Status", 200);
        messageSuccess.put("Message", "Book price calculation is success");
        return messageSuccess;
    }
}