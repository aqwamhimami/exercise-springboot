package springboottutorial.springboottutorial.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "authors")
public class Author implements Serializable {
    /**
	 *
	 */
	private static final long serialVersionUID = 3636618938390098156L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
	private String firstName;
	
	private String lastName;
	
	@OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
	private Set<Book> books;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}