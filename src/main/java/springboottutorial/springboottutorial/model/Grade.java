package springboottutorial.springboottutorial.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "grade")
public class Grade implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String quality;

    @NotBlank
	private Double baseProduction;
	
	@OneToMany(mappedBy = "grade", cascade = CascadeType.ALL)
	private Set<Publisher> publishers;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public Double getBaseProduction() {
		return baseProduction;
	}

	public void setBaseProduction(Double baseProduction) {
		this.baseProduction = baseProduction;
	}
	
}